<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserStoreRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Service\User\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct(public UserService $userService){}


    /**
     * @param UserStoreRequest $request
     * @return mixed
     */
    public function store(UserStoreRequest $request)
    {
        $input = $request->only('name','email');
        $input['password'] = Hash::make($request->get('password'));
        return $this->userService->create($input);
    }


    /**
     * @param string $id
     * @param UserUpdateRequest $request
     * @return mixed
     */
    public function update(string $id, UserUpdateRequest $request)
    {
        $input = $request->only('name');
        $input['password'] = Hash::make($request->get('password'));
        return $this->userService->update($id, $input);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function delete(string $id)
    {
        return $this->userService->delete($id);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->userService->index();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->userService->show($id);
    }
}
