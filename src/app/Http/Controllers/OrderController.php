<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\OrderStoreRequest;
use App\Service\Order\OrderProductService;
use App\Service\Order\OrderService;
use App\Service\Product\ProductService;

class OrderController extends Controller
{
    public function __construct(public OrderService $orderService,public ProductService $productService,public OrderProductService $orderProductService){}

    public function store(OrderStoreRequest $request)
    {
        $products = $request->get('product');
        foreach ($products as $product){
            if ($this->checkCount($product['count'],$product['id']) == false) return 'تعداد محصول مورد نظر موجود نمی باشد';
        }
        $order = $this->orderService->create($request->only('user_id'));
        foreach ($products as $product){
            $this->orderProductService->create([
                'product_id' => $product['id'],
                'order_id' => $request[$order['id']],
            ]);
            $productCount = $this->productService->show($product['id']);
            $this->productService->update($product['id'], ['count' => $productCount['count'] - $product['count']]);
        }
        return response()->json('ok:)');
    }
    public function checkCount(int $count,string $product)
    {
        return $this->productService->checkCount($count, $product);
    }
}
