<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductStoreRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Service\Product\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct(public ProductService $productService){}


    /**
     * @param ProductStoreRequest $request
     * @return mixed
     */
    public function store(ProductStoreRequest $request): mixed
    {
        return $this->productService->create($request->all());
    }


    /**
     * @param int $id
     * @param ProductUpdateRequest $request
     * @return mixed
     */
    public function update(int $id, ProductUpdateRequest $request): mixed
    {
        return $this->productService->update($id, $request->all());
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->productService->delete($id);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->productService->index();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->productService->show($id);
    }
}
