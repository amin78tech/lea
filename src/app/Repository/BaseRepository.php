<?php

namespace App\Repository;

class BaseRepository
{
    /**
     * @param $model
     */
    public function __construct(public $model){}

    /**
     * @param array $input
     * @return mixed
     */
    public function create(array $input)
    {
        return $this->model->query()->create($input);
    }

    /**
     * @param string $uuid
     * @param array $input
     * @return mixed
     */
    public function update(string $uuid, array $input)
    {
        return $this->model->query()->where('_id',$uuid)->update($input);
    }

    /**
     * @param string $uuid
     * @return mixed
     */
    public function delete(string $uuid)
    {
        return $this->model->query()->where('_id', $uuid)->delete();
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->model->query()->get();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->model->query()->where('_id',$id)->first();
    }
}
