<?php

namespace App\Repository\Order;

use App\Models\Order;
use App\Repository\BaseRepository;

class OrderRepository extends BaseRepository
{
    public function __construct(Order $model)
    {
        parent::__construct($model);
    }
}
