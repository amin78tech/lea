<?php

namespace App\Repository\Order;

use App\Models\OrderProduct;
use App\Repository\BaseRepository;

class OrderProductRepository extends BaseRepository
{
    public function __construct(OrderProduct $model)
    {
        parent::__construct($model);
    }
}
