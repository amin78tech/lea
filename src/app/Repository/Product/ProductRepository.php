<?php

namespace App\Repository\Product;

use App\Models\Product;
use App\Repository\BaseRepository;

class ProductRepository extends BaseRepository
{
    public function __construct(Product $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $count
     * @param string $product
     * @return bool
     */
    public function checkCount(int $count, string $product): bool
    {
        $product = $this->model->query()->where('_id', $product)->first();
        return $product['count'] >= $count ? true : false;
    }
}
