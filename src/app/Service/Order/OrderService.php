<?php

namespace App\Service\Order;

use App\Models\Order;
use App\Repository\BaseRepository;
use App\Repository\Order\OrderRepository;
use App\Service\BaseService;

class OrderService extends BaseService
{
    public function __construct(OrderRepository $repository)
    {
        parent::__construct($repository);
    }
}
