<?php

namespace App\Service\Order;

use App\Repository\Order\OrderProductRepository;
use App\Service\BaseService;

class OrderProductService extends BaseService
{
    public function __construct(OrderProductRepository $repository)
    {
        parent::__construct($repository);
    }
}
