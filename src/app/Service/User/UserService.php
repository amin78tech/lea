<?php

namespace App\Service\User;

use App\Repository\User\UserRepository;
use App\Service\BaseService;

class UserService extends BaseService
{
    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }
}
