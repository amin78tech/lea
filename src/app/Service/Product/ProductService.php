<?php

namespace App\Service\Product;

use App\Repository\Product\ProductRepository;
use App\Service\BaseService;

class ProductService extends BaseService
{
    public function __construct(ProductRepository $repository)
    {
        parent::__construct($repository);
    }
    public function checkCount(int $count,string $product)
    {
        return $this->repository->checkCount($count, $product);
    }
}
