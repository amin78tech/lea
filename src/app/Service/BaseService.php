<?php

namespace App\Service;

class BaseService
{
    /**
     * @param $repository
     */
    public function __construct(public $repository){}

    /**
     * @param array $input
     * @return mixed
     */
    public function create(array $input): mixed
    {
        return $this->repository->create($input);
    }


    /**
     * @param string $uuid
     * @param array $input
     * @return mixed
     */
    public function update(string $uuid, array $input): mixed
    {
        return $this->repository->update($uuid, $input);
    }


    /**
     * @param string $uuid
     * @return mixed
     */
    public function delete(string $uuid): mixed
    {
        return $this->repository->delete($uuid);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->index();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->repository->show($id);
    }

}
